# Ships

## Configuration

### Diamondback explorer

- [Basic Exploration Mode](https://s.orbis.zone/c2wl)

### Type 9

- [Basic Laser Mining Mode](https://s.orbis.zone/c2xu)

## Powerplants

|Module/Type|Priority|Notes|
|---|---|---|
|Thrusters|1||
|FSD|1||
|Life Support|2||
|Sensors|2||
|Shields|3||
|Shield Boosters|3||
|Weapons|4||
|Defense Utilities|5|Chaff, KWS, ECM, Heat Sinks, Shield Cell Banks etc.|
|Other Utilities|5|FSD Booster, FSD Interdictor, Fuel Scoop etc.|
|Flight Assists|5|Docking Computer, Supercruise Assist|
|Cargo|5|Cargo Hatch, Limpet Controllers| 


Original Information can be found [here](https://redshiftlogistics.online/2020/02/27/optimising-powerplant-output.html)