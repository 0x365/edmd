# Materiales

Esta seccion describe las diferentes formas de obtener materiales de manera detallada, adicionalmente hace enlace a las fuente de donde esta informacion puede ser corroborada.

## General:

* [Video de CMDR Exigeous](https://youtu.be/-4HClk1cRIo) explicando de manera detallada como obtener los diferentes materiales.
  * En el [segundo 32](https://youtu.be/-4HClk1cRIo?t=32) explican como obtener materiales codificados.
  * En el [minuto 1:50](https://youtu.be/-4HClk1cRIo?t=110) explican como obtener minerales.
  * En el [minuto 3](https://youtu.be/-4HClk1cRIo?t=180) explica como obtener materiales manufacturados.

## Datos:

- En este [sitio web](https://redshiftlogistics.online/2020/02/15/data-farm-1.html) se puede encontrar una explicacion detallada de como minar.

## Minerales

- Viajar a los siguientes sistemas:
  - HIP 36601:
    - planeta c-5a señal Biologica (8) tecnecio
    - planeta c-1a señal Biologica (2) polonio
    - planeta c-1d señal Biologica (6) rutenio
    - planeta c-3b señal Biologica (4) teluro
  - Outotz LS-K d8-3 
    - planeta B-5a  Itrio
    - planeta B-5c  Antiomonio

## Manufacturados

- Entrar en solo.
- En cualquier sistema ir al nav beacon.
- Buscar una señal de grado alto que tenga al menos 15 minutos de preferencia.
- Entrar recoger materiales.
- SALIR DEL JUEGO
- Entrar de nuevo a solo y entrar en supercrucero.
- Inmediatamente tendras una señal cerca de ti, vuelve a entrar y repite hasta que la señal se vaya o llenes el material.

### Recomendaciones

#### Datos:
- Dejar en Diaguandri una sidewinder para ir a obtener estos materiales.
- Al terminar de obtener los datos explotamos la sidewinder y aparecemos inmediatamente en el material trader.
- Ahorro promedio de tiempo 10 minutos.

#### Minerales
- Irse en una Diamon Black Explorer
- Al terminar de obtener todos los minerales explotamos y aparecemos en la burbuja.
- Ahorro promedio de tiempo 30 minutos.