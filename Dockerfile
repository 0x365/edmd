  FROM node:latest

  LABEL description="This container have Elite Dangerous notes."
  COPY . .
  WORKDIR /docs
  RUN npm install -g docsify-cli@latest
  EXPOSE 3000/tcp
  ENTRYPOINT docsify serve .